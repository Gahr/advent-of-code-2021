body = File.read!("input.txt")
    |> String.trim
    |> String.split("\n")
    |> Enum.map(&String.to_integer/1)
    |> Enum.chunk_every(3, 1, :discard)
    |> Enum.map(&Enum.sum/1)

body
    |> Enum.with_index
    |> Enum.drop(1)
    |> Enum.reduce(0, fn({number, index}, acc) ->
        previous = Enum.at(body, index - 1)
        if number > previous, do: acc + 1, else: acc
    end)
    |> IO.puts
