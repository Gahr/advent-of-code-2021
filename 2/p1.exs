File.read!("input")
    |> String.trim
    |> String.split("\n")
    |> Enum.map(fn c ->
        [command | [x | _]] = String.split(c)
        {command, String.to_integer(x)}
    end)
    |> Enum.reduce({0, 0}, fn(command, {horizontal, depth}) ->
        case command do
            {"forward", x} ->
                {horizontal + x, depth}
            {"down", x} ->
                {horizontal, depth + x}
            {"up", x} ->
                {horizontal, depth - x}
        end
    end)
    |> fn {horizontal, depth} -> horizontal * depth end.()
    |> IO.puts
