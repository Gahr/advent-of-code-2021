File.read!("input")
    |> String.trim
    |> String.split("\n")
    |> Enum.map(fn c ->
        [command | [x | _]] = String.split(c)
        {command, String.to_integer(x)}
    end)
    |> Enum.reduce({0, 0, 0}, fn(command, {horizontal, depth, aim}) ->
        case command do
            {"forward", x} ->
                {horizontal + x, depth + aim * x, aim}
            {"down", x} ->
                {horizontal, depth, aim + x}
            {"up", x} ->
                {horizontal, depth, aim - x}
        end
    end)
    |> fn {horizontal, depth, _} -> horizontal * depth end.()
    |> IO.puts
